package com.daioware.net.ssl;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.daioware.file.FileUtil;

public class SSLManager implements Iterable<SSLItem>{
	public static final String EXTS[]= {".jks"};
	private HashMap<String,SSLItem> sslItems;
	
	public SSLManager() {
		sslItems=new HashMap<>();
	}
	public SSLManager(File folder) {
		this();
		for(File certFolder:folder.listFiles()) {
			if(certFolder.isDirectory()) {
				List<File> files=FileUtil.getFiles(certFolder, EXTS);
				if(files.size()>=1) {
					try {
						addSSLItem(new SSLItem(certFolder.getName(),files.get(0),
								FileUtil.getContents(certFolder.getAbsolutePath()
										+File.separator+"pwd.txt",Charset.forName("utf-8"))));
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	public void addSSLItems(List<SSLItem> items) {
		items.forEach((s)->addSSLItem(s));
	}
	public void addSSLItem(SSLItem... items) {
		for(SSLItem it:items) {
			addSSLItem(it);
		}
	}
	public SSLItem get(String name) {
		return sslItems.get(name);
	}
	public SSLItem removeSSLItem(String name) {
		return sslItems.remove(name);
	}
	public SSLItem addSSLItem(SSLItem s) {
		return sslItems.put(s.getName(), s);
	}
	@Override
	public Iterator<SSLItem> iterator() {
		return sslItems.values().iterator();
	}
	
	public static void main(String[] args) {
		SSLManager manager=new SSLManager(new File("C:\\Users\\Diego Olvera\\Documents\\dev\\ssl\\"));
		for(SSLItem sslItem:manager) {
			System.out.println(sslItem);
		}
	}
	
}
