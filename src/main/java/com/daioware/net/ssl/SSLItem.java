package com.daioware.net.ssl;

import java.io.File;

public class SSLItem {
	private String name;
	private File keyFile;
	private String password;
		
	public SSLItem(String name, File keyFile, String password) {
		setName(name);
		setKeyFile(keyFile);
		setPassword(password);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public File getKeyFile() {
		return keyFile;
	}
	public void setKeyFile(File file) {
		this.keyFile = file;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof SSLItem))
			return false;
		SSLItem other = (SSLItem) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	public String toString() {
		StringBuilder info=new StringBuilder();
		info.append("name:").append(getName()).append("\n");
		info.append("keyFile:").append(getKeyFile().getAbsolutePath()).append("\n");
		info.append("pwd:").append(getPassword()).append("\n");
		return info.toString();
	}
	
}
